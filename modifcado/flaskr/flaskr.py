# -*- coding: utf-8 -*-
"""
    Flaskr
    ~~~~~~

    A microblog example application written as Flask tutorial with
    Flask and sqlite3.

    :copyright: (c) 2015 by Armin Ronacher.
    :license: BSD, see LICENSE for more details.
"""
from datetime import datetime

from flask import (Flask, abort, flash, redirect, render_template, request,
                   session, url_for)
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager, Server
from flask_sqlalchemy import SQLAlchemy
from pypugjs.ext.jinja import PyPugJSExtension

# create our little application :)
app = Flask(__name__)
app.jinja_env.add_extension(PyPugJSExtension)

# Load default config and override config from an environment variable
app.config.update(dict(
    SQLALCHEMY_DATABASE_URI='sqlite:///flaskr.db',
    SQLALCHEMY_ECHO=False,
    SQLALCHEMY_TRACK_MODIFICATIONS=False,
    DEBUG=True,
    SECRET_KEY='development key',
    USERNAME='admin',
    PASSWORD='default'
))
app.config.from_envvar('FLASKR_SETTINGS', silent=True)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
manager = Manager(app)
manager.add_command('db', MigrateCommand)
manager.add_command("runserver", Server())


class Entries(db.Model):
    __tablename__ = 'entries'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(60))
    text = db.Column(db.String)
    pub_date = db.Column(db.DateTime)

    def __init__(self, title, text):
        self.title = title
        self.text = text
        self.pub_date = datetime.utcnow()


@app.route('/')
def show():
    entries = Entries.query.order_by(Entries.pub_date.desc()).all()
    return render_template('show.pug', entries=entries)


@app.route('/new', methods=['GET', 'POST'])
def new():
    if not session.get('logged_in'):
        abort(401)
    if request.method == 'POST':
        if not request.form['title']:
            flash('Title is required', 'error')
        elif not request.form['text']:
            flash('Text is required', 'error')
        else:
            entrie = Entries(request.form['title'], request.form['text'])
            db.session.add(entrie)
            db.session.commit()
            flash(u'Entry was successfully created')
            return redirect(url_for('show'))
        flash('New entry was successfully posted')
    return render_template('new.pug')


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        if request.form['username'] != app.config['USERNAME']:
            flash('Invalid username')
        elif request.form['password'] != app.config['PASSWORD']:
            flash('Invalid password')
        else:
            session['logged_in'] = True
            flash('You were logged in')
            return redirect(url_for('show'))
    return render_template('login.pug')


@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(url_for('show'))


if __name__ == '__main__':
    manager.run()
